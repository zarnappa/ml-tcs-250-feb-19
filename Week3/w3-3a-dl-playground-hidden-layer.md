Deep Learning Playground
=======================
# Setup

## Introducing the playground

  * Navigate in your browser to http://playground.tensorflow.org
  * This is a playground that we will use to play with some concepts
  * It will be fun!

---
## Start

  * When you start, you should see this:

![](./images/playground-startup.png) <!-- {"left" : 1.02, "top" : 1.6, "height" : 5.41, "width" : 8.21} -->


Notes: 


---

## Hidden Layers
  * Feature selection can help us
    - Can model non-linear decision boundary
  * Add a hidden layer

![](./images/playground-hidden-layer.png) <!-- {"left" : 2.04, "top" : 2.62, "height" : 4.62, "width" : 6.18} -->


Notes: 

---
## Circle Dataset With Hidden Layers
  * Select the circle dataset:

![](./images/playground-dataset-circle.png)   <!-- {"left" : 2.64, "top" : 1.75, "height" : 4.99, "width" : 4.97} -->

## Circle Dataset With Hidden Layers

  * Select only X1 and X2 as features
  * Add a Hidden Layer

![](./images/playground-hidden-layer.png) <!-- {"left" : 6.13, "top" : 0.96, "height" : 2.57, "width" : 3.72} -->

  * Can you get a solution with 1 hidden Layer
    - You can add neurons.



Notes: 

---
## Four Square Dataset Hidden Layers 
   * Set the Four-Square dataset:

![](./images/playground-dataset-fourposter.png) <!-- {"left" : 0.77, "top" : 1.38, "height" : 4.06, "width" : 4.07} -->


   * Can you solve it with hidden layers?

![](./images/playground-hidden-layer.png) <!-- {"left" : 6.67, "top" : 5.09, "height" : 2.13, "width" : 2.85} -->



Notes: 

---
## Spiral

   * Set the Spiral dataset:

![](./images/playground-dataset-spiral.png)  <!-- {"left" : 6.08, "top" : 1.09, "height" : 3.65, "width" : 3.61} -->


   * This one is really tricky!
   * Can you do it?
   * Don't be afraid to add new features!
   * Multiple Hidden Layers?


Notes: 

---
## Spiral Solution
   * Can you get this result?

![](./images/playground-dataset-spiral-solution.png) <!-- {"left" : 4.94, "top" : 0.97, "height" : 6.41, "width" : 5.05} -->


   
Notes: 


